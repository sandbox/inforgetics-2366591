<?php

/**
 * @file
 * Field hooks to implement a CPM indicator field.
 */

/**
 * Implements hook_field_formatter_info().
 */
function cpm_indicator_field_formatter_info() {
  $formatters = array(
    'cpmindicator_text_icon_formatter' =>  array(
      'label' => t('Text and Color/Icon'),
      'field types' => array('cpmindicator'),
      'settings' => array(
        'container' => array(
          'float' => 'right',
          'class' => '',
          'inline_style' => '',
//          array('background' =>
//            array('image' => ''),
//          ),
        ),
//        'cpmindicator_part' => array(
//           'show_trend' => TRUE,
//           'show_comment' => TRUE,
//           'inline_style_trend' => '',
//           'inline_style_comment' => '',
//        ),
        'media' => 'colors', // background color of icon area
      ),
    ),
    'cpmindicator_icon_only_formatter' => array(
      'label' => t('Color or Icon'),
      'field types' => array('cpmindicator'),
      'settings' => array(
        'container' => array(
          'float' => 'right',
          'class' => '',
          'inline_style' => '',
//          array('background' =>
//            array('image' => ''),
//          ),
          ),
        'media' => 'colors', // background color of icon area
        ),
    ),
    'cpmindicator_icon_comment_formatter' => array(
      'label' => t('Color or Icon + comment'),
      'field types' => array('cpmindicator'),
      'settings' => array(
        'container' => array(
          'float' => 'right',
          'class' => '',
          'inline_style' => '',
//          array('background' =>
//            array('image' => ''),
//          ),
          ),
        'media' => 'colors', // background color of icon area
        ),
    ),
    'cpmindicator_text_only_formatter' => array(
      'label' => t('Text'),
      'field types' => array('cpmindicator'),
      'settings' => array(
        'container' => array(
          'float' => 'right',
          'class' => '',
          'inline_style' => '',
//          array('background' =>
//            array('image' => ''),
//          ),
          ),
        'media' => 'colors', // background color of icon area
        ),
    ),
    'virtual_default_formatter' =>  array(
      'label' => t('Virtual field'),
      'field types' => array('virtual'),
      'settings' => array(
        'container' => array(
          'float' => 'right',
          'class' => '',
          'inline_style' => '',
//          array('background' =>
//            array('image' => ''),
//          ),
        ),
      ),
    ),
    'cpmindicator_comment_formatter' =>  array(
      'label' => t('Indicator Comment as Icon'),
      'field types' => array('text', 'text_long'),
      'weight' => 10,
      'settings' => array(
        'container' => array(
          'float' => 'right',
          'class' => '',
//          'inline_style' => '',
//          array('background' =>
//            array('image' => ''),
//          ),
        ),
      ),
    ),
  );
  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function cpm_indicator_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $formatter = $display['type'];
  module_load_include('inc', 'cpm_indicator', 'cpm_indicator_admin');
  switch ($formatter) {
    case 'cpmindicator_text_icon_formatter':
    case 'cpmindicator_icon_comment_formatter':
    case 'cpmindicator_icon_only_formatter':
      $form = cpmindicator_icon_formatter_settings_form($field, $instance, $view_mode, $form, $form_state);
      break;
//    case 'cpmindicator_comment_formatter':
//      $form = cpmindicator_comment_formatter_settings_form($field, $instance, $view_mode, $form, $form_state);
//      break;
//    case 'virtual_default_formatter':
//      $form = virtual_default_formatter_settings_form($field, $instance, $view_mode, $form, $form_state);
//      break;
  }
  $context = array(
    'field' => $field,
    'instance' => $instance,
    'view_mode' => $view_mode,
  );
  drupal_alter('cpm_indicator_field_formatter_settings_form', $form, $form_state, $context);
  return $form;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function cpm_indicator_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $formatter = $display['type'];
  module_load_include('inc', 'cpm_indicator', 'cpm_indicator_admin');
  $summary=array();
  switch ($formatter) {
    case 'cpmindicator_text_icon_formatter':
    case 'cpmindicator_icon_comment_formatter':
    case 'cpmindicator_icon_only_formatter':
      $summary = cpmindicator_icon_formatter_settings_summary($field, $instance, $view_mode);
      break;
    case 'virtual_default_formatter':
      $summary = virtual_default_formatter_settings_summary($field, $instance, $view_mode);
      break;
    case 'cpmindicator_comment_formatter':
      $summary = cpmindicator_comment_formatter_settings_summary($field, $instance, $view_mode);
      break;
  }
  $context = array(
    'field' => $field,
    'instance' => $instance,
    'view_mode' => $view_mode,
  );
  drupal_alter('cpm_indicator_field_formatter_settings_summary', $summary, $context);

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 *
 * Useful values:
 *
 *   $entity->date_id
 *     If set, this will show only an individual date on a field with
 *     multiple dates. The value should be a string that contains
 *     the following values, separated with periods:
 *     - module name of the module adding the item
 *     - node nid
 *     - field name
 *     - delta value of the field to be displayed
 *     - other information the module's custom theme might need
 *
 *     Used by the calendar module and available for other uses.
 *     example: 'date:217:field_date:3:test'
 *
 *   $entity->date_repeat_show
 *     If true, tells the theme to show all the computed values
 *     of a repeating date. If not true or not set, only the
 *     start date and the repeat rule will be displayed.
 */
function cpm_indicator_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];
  $formatter = $display['type'];
  $variables = array(
    'entity' => $entity,
    'entity_type' => $entity_type,
    'field' => $field,
    'instance' => $instance,
    'langcode' => $langcode,
    'items' => $items,
    'display' => $display,
    'cpmindicators' => array(),
    'attributes' => array(),
  );

  // Give other modules a chance to prepare the entity before formatting it.
  drupal_alter('cpm_indicator_formatter_pre_view', $entity, $variables);

  // See if we are only supposed to display a selected
  // item from multiple value cpmindicator fields.
  $selected_deltas = array();
  if (!empty($entity->cpmindicator_id)) {
    foreach ((array) $entity->cpmindicator_id as $key => $id) {
      list($module, $nid, $field_name, $selected_delta, $other) = explode('.', $id . '.');
      if ($field_name == $field['field_name']) {
        $selected_deltas[] = $selected_delta;
      }
    }
  }

  switch ($display['type']) {
    case 'virtual_default_formatter':
      foreach ($items as $delta => $item) {
          $variables['delta'] = $delta;
          $variables['item'] = $item;
          $variables['comments']=&drupal_static('cpmindicator_comments_container',array());
          $variables['attributes'] = array();
          $output = theme('virtual_display_comments', $variables);
          if (!empty($output)) {
            $element[$delta] = array('#markup' => $output);
          }
      }
      break;
    case 'cpmindicator_comment_formatter':
      foreach ($items as $delta => $item) {
        $value = _text_sanitize($instance, $langcode, $item, 'value');
        $variables['delta'] = $delta;
        $variables['value'] = $value;
        $variables['attributes'] = array();
        $output = theme('cpmindicator_display_comment', $variables);

        if (!empty($output)) {
          $element[$delta] = array('#markup' => $output);
        }
      }
      break;
    default:
      foreach ($items as $delta => $item) {
        if (!empty($entity->cpmindicator_id) && !in_array($delta, $selected_deltas)) {
          continue;
        }
        else {
          $variables['delta'] = $delta;
          $variables['item'] = $item;
          $variables['attributes'] = array();
          $output = theme('cpmindicator_display_value_trend', $variables);
          if (!empty($output)) {
            $element[$delta] = array('#markup' => $output);
          }
        }
      }
  }
  return $element;
}

/**
 * Implements hook_field_is_empty().
 */
function cpm_indicator_field_is_empty($item, $field) {
  if (empty($item['value'])) {
    return TRUE;
  }
  elseif ($field['settings']['trend'] == 'required' && empty($item['trend'])) {
    return TRUE;
  }
  elseif ($field['settings']['comment'] == 'required' && empty($item['comment_value'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_info().
 */
function cpm_indicator_field_info() {
  $settings = array(
    'settings' => array(
      'value_set' => 'smilies',
      'trend' => 'optional',
      'comment' => 'optional',
      'entity_translation_sync' => array('value', 'trend'),
    ),
    'instance_settings' => array(
      'text_processing' => 0,
      'entity_translation_sync' => FALSE,
    ),
  );
  $field_info = array(
    'cpmindicator' => array(
      'label' => t('Indicator'),
      'description' => t('CPM Indicator'),
      'default_widget' => 'cpm_widget_radios',
      'default_formatter' => 'cpmindicator_default_formatter',
      ) + $settings,
  );

  $field_info += array(
    'virtual' => array(
      'label' => t('Virtual field'),
      'description' => t('Calculated field'),
      'default_widget' => 'hidden',
      'default_formatter' => 'virtual_default_formatter',
      'module' => 'virtual_field',
      'virtual_field' => array(
        'entity_types' => array('node','smilecheck'),
        'add_widget' => true,
        ),
      ),
    );
  return $field_info;
}

/**
 * Implements hook_field_widget_info().
 */
function cpm_indicator_field_widget_info() {
  $settings = array(
    'settings' => array(
//      'value_set' => 'smilies',
      'label_position' => 'above',
    ),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_DEFAULT,
      'default value' => FIELD_BEHAVIOR_NONE,
    ),
  );

  $info = array(
    'cpm_widget_radios' => array(
      'label' =>  t('Radios'),
      'field types' => array('cpmindicator'),
    ) + $settings,
    'cpm_widget_radio_icons' => array(
      'label' =>  t('Icons'),
      'field types' => array('cpmindicator'),
     ) + $settings,
    'cpm_widget_select' => array(
      'label' =>  t('Select'),
      'field types' => array('cpmindicator'),
     ) + $settings,
    'cpm_widget_select_icons' => array(
      'label' =>  t('Select with icons'),
      'field types' => array('cpmindicator'),
     ) + $settings,
  );

  return $info;
}

/**
 * Implements hook_field_load().
 *
 * Where possible, generate the sanitized version of each field early so that
 * it is cached in the field cache. This avoids looking up from the filter cache
 * separately.
 *
 * @see text_field_formatter_view()
 */
function cpm_indicator_field_load($entity_type, $entities, $field, $instances, $langcode, &$items) {
  foreach ($entities as $id => $entity) {

    foreach ($items[$id] as $delta => $item) {
      // Only process items with a cacheable format, the rest will be handled
      // by formatters if needed.
      if (empty($instances[$id]['settings']['text_processing']) || filter_format_allowcache($item['comment_format'])) {
        $items[$id][$delta]['safe_value'] = isset($item['comment_value']) ? _text_sanitize($instances[$id], $langcode, $item, 'comment_value') : '';
      }
    }
    if ($field['field_name'] == 'field_cpmindicator_comments') {
      $items[$id][0]['cpmindicator_comments'] = 'no comments';
    }
  }
}

/**
 * Implements hook_field_instance_settings_form().
 *
 * Wrapper functions for cpm_indicator administration, included only when processing
 * field settings.
 */
function cpm_indicator_field_instance_settings_form($field, $instance) {
  switch ($field['type']) {
  case 'cpmindicator':
    module_load_include('inc', 'cpm_indicator', 'cpm_indicator_admin');
    return _cpmindicator_field_instance_settings_form($field, $instance);
    break;
  default:
    return array();
  }
}

/**
 * Implements hook_field_widget_settings_form().
 */
function cpm_indicator_field_widget_settings_form($field, $instance) {
  switch ($field['type']) {
  case 'cpmindicator':
    module_load_include('inc', 'cpm_indicator', 'cpm_indicator_admin');
    return _cpmindicator_field_widget_settings_form($field, $instance);
    break;
  default:
    return array();
  }
}

/**
 * Implements hook_field_settings_form().
 */
function cpm_indicator_field_settings_form($field, $instance, $has_data) {
  switch ($field['type']) {
  case 'cpmindicator':
    module_load_include('inc', 'cpm_indicator', 'cpm_indicator_admin');
    return _cpmindicator_field_settings_form($field, $instance, $has_data);
    break;
  default:
    return array();
  }
}

