CPM Indicator Feeds Mapper

A mapper for the feeds module. This allows importing cpm indicators.

Required Modules:

    CPM Indicator
    Feeds

To use this module structure a CSV file as follows:
id 	title 	technical:value 	technical:trend 	technical:comment
1001 	Parent A 	
1002 	Parent B 	
1003 	Child A1 	1001
1004 	Child A2 	1001
1005 	Child B1 	1002
1006 	Child B2 	1002
1003 	Grand Child A11 	1003
1003 	Grand Child A12 	1003

Create a new Feeds Importer with the following settings:

    Fetcher: File upload
    Parser: CSV parser
    Processor: Node processor

Add the mappings:
Source 	Target 	Unique Target
id 	GUID 	Yes
title 	Title 	No
technical:value 	technical:value 	No
technical:trend 	technical:trend 	No
technical:comment 	technical:comment 	No


Minimally tested on a standard install of Drupal 7.32

Trends: up, neutral, down
Smilecheck: TOP, OK, NOK, KO
