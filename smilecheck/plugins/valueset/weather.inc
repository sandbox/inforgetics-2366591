<?php

/**
 * @file
 * Weather valueset
 */

$plugin = array(
  'title' => 'Weather',
  'values' => array(
    'description' => t('the project is'),
    'options' => array(
      'shiny' => array (
        'int' => 2,
        'name' => t('Shiny'),
        'desc' => t('marked by an event particularly positive'),
        'color' => '#66CCFF',
        'icon' => 'weather/shiny.gif',
      ),
      'cloudy' => array (
        'int' => 1,
        'name' => t('Cloudy'),
        'desc' => t('conform to expectations'),
        'color' => '#009900',
        'icon' => 'weather/cloudy.gif',
      ),
      'foggy' => array (
        'int' => -1,
        'name' => t('Foggy'),
        'desc' => t('facing questions or incertainties perturbing advancement'),
        'color' => '#FF9933',
        'icon' => 'weather/cloudy.gif',
      ),
      'stormy' => array (
        'int' => -2,
        'name' => t('Stormy'),
        'desc' => t('facing problems that require urgent action or decision'),
        'color' => '#FF0033',
        'icon' => 'weather/stormy.gif',
      ),
    ),
  ),
  'trends' => array(
    'description' => t('and the trend is'),
    'options' => array(
      'up' => array (
        'int' => 2,
        'name' => t('brighter'),
        'desc' => t('Good or better news are coming.'),
        'color' => '#339966',
        'icon' => 'trends/evol_UP.png',
        'bkgrnd' => 'trends/up.gif',
      ),
      'neutral' => array (
        'int' => 1,
        'name' => t('equal'),
        'desc' => t('Current situation should go on.'),
        'color' => '#BBBBBB',
        'icon' => 'trends/evol_FLAT.png',
        'bkgrnd' => 'trends/neutral.gif',
      ),
      'down' => array (
        'int' => -1,
        'name' => t('darker'),
        'desc' => t('Worse news are expected if conditions do not change.'),
        'color' => '#CC3333',
        'icon' => 'trends/evol_DOWN.png',
        'bkgrnd' => 'trends/down.gif',
      ),
    ),
  ),
);
