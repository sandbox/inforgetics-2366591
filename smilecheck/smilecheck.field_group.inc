<?php
/**
 * @file
 * smilecheck.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function smilecheck_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cartridge|node|smilecheck|default';
  $field_group->group_name = 'group_cartridge';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'smilecheck';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '0',
    'children' => array(
      0 => 'field_project_ref',
      1 => 'field_period_ref',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-cartridge field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups[''] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_legend|node|smilecheck|form';
  $field_group->group_name = 'group_legend';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'smilecheck';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Legend',
    'weight' => '3',
    'children' => array(
      0 => 'field_legend',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Legend',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-legend field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups[''] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Legend');

  return $field_groups;
}
