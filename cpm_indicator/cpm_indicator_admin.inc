<?php

/**
 * @file
 * cpm_indicator administration code.
 */

/**
 * Settings for the text and icon formatter.
 */
function cpmindicator_icon_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $formatter = $display['type'];
  $value_set = $field['settings']['value_set'];
  $form = array();

  $media_options = array();
  $valueset = cpm_indicator_value_set($bundle, $value_set);
  foreach($valueset['media'] AS $name => $media){
    $media_options[$name] = $media['title'];
  }
  
  $form['media'] = array(
    '#title' => t('Icon set'),
    '#type' => 'select',
    '#options' => $media_options,
    '#default_value' => isset($settings['media']) ? $settings['media'] : 'colors',
    '#description' => t('Choose how to display the indicator'),
    '#weight' => 0,
  );
  return $form;
}

/**
 * Settings summary for the text and icon formatter.
 */
function cpmindicator_icon_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $formatter = $display['type'];
  $summary = array();
/*
  $options = array(
    'colors' => t('Colors'),
    'smilies' => t('Smilies'),
    'weather' => t('Weather'),
  );
  switch ($formatter) {
    case 'cpmindicator_text_icon_formatter':
      $format = $options[$settings['media']];
      $summary[] = t('Display indicators as text + icon using the @format analogy', array('@format' => $format));
      break;
    case 'cpmindicator_icon_only_formatter':
      $format = $options[$settings['media']];
      $summary[] = t('Display indicators as icon using the @format analogy', array('@format' => $format));
    break;
    default:
  }
*/
  switch ($display['type']) {
    case 'cpmindicator_text_icon_formatter':
      $show_desc = t('detailled with text and icon');
      break;
    case 'cpmindicator_icon_only_formatter':
      $show_desc = t('in a compact mode, just with icon');
      break;
    case 'cpmindicator_text_only_formatter':
      $show_desc = t('as simple text');
      break;
    case 'cpmindicator_icon_comment_formatter':
      $show_desc = t('as cards with icon and comments');
      break;
  }
  $summary[] = t('Display indicator value with %media %show', array('%media' => $settings['media'], '%show'=>$show_desc));
  return $summary;
}

/**
 * Settings summary for the virtual field formatter (used for trends).
 */
function virtual_default_formatter_settings_summary($field, $instance, $view_mode) {
  $summary = array();
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $formatter = $display['type'];
  $summary[] = t('Virtual field is fully managed from code');

  return $summary;
}

/**
 * Settings summary for the comment field formatter.
 */
function cpmindicator_comment_formatter_settings_summary($field, $instance, $view_mode) {
  $summary = array();
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $formatter = $display['type'];
  $summary[] = t('Replace text by Icon and display through Tooltip or Footnote');

  return $summary;
}
/**
 * Helper function for cpm_indicator_field_settings_form().
 *
 * @see cpm_indicator_field_settings_validate()
 */
function _cpmindicator_field_settings_form($field, $instance, $has_data) {
  $settings = $field['settings'];

  $form = array(
    '#element_validate' => array('cpm_indicator_field_settings_validate'),
  );
  $bundle=$instance['bundle'];
  $type=$instance['widget']['type'];

  $value_set_options = array();
  foreach (cpm_indicator_value_set($bundle) AS $name => $info){
    $value_set_options[$name] = t($info['title']);
  }

  $form['value_box'] = array(
    '#type' => 'fieldset',
    '#title' => t('Value set'),
  );
  $description = t('State options for current situation');
  $form['value_box']['value_set'] = array(
    '#type' => 'select',
    '#title' => t('Select of set of options'),
    '#description' => $description,
    '#options' => $value_set_options,
    '#default_value' => (empty($settings['value_set']) ? 'smilies' : $settings['value_set']),
    '#disabled' => $has_data,
  );
  $form['trend_box'] = array(
    '#type' => 'fieldset',
    '#title' => t('Trend'),
  );
  $description = t('Trend is used to share vision of the future');
  $form['trend_box']['trend_get'] = array(
    '#type' => 'checkbox',
    '#title' => t('Collect a trend'),
    '#description' => $description,
    '#default_value' => (empty($settings['trend']) ? FALSE : TRUE),
    '#disabled' => $has_data,
  );
  $form['trend_box']['trend_required'] = array(
    '#type' => 'checkbox',
    '#title' => t('Required'),
    '#default_value' => ((isset($settings['trend']) && $settings['trend'] === 'required') ? TRUE : FALSE),
//    '#disabled' => $has_data,
    '#states' => array(
      'invisible' => array(
        'input[name="field[settings][trend_box][trend_get]"]' => array('checked' => FALSE),
      ),
    ),
  );
  $form['comment_box'] = array(
    '#type' => 'fieldset',
    '#title' => t('Comment'),
  );
  $description = t('Complete chosen value with short comment');
  $form['comment_box']['comment_get'] = array(
    '#type' => 'checkbox',
    '#title' => t('Collect a comment'),
    '#description' => $description,
    '#default_value' => (empty($settings['comment']) ? FALSE : TRUE),
    '#disabled' => $has_data,
  );
  $comment_required_options = array('optional' => t('No'), 'required' => t('Yes'), 'if_not_ok' => t('According to value set definition'));
  $form['comment_box']['comment_required'] = array(
    '#type' => 'select',
    '#title' => t('Required'),
    '#options' => $comment_required_options,
    '#default_value' => (isset($settings['comment']) ? $settings['comment'] : 'if_not_ok'),
//    '#disabled' => $has_data,
    '#states' => array(
      'invisible' => array(
        'input[name="field[settings][comment_box][comment_get]"]' => array('checked' => FALSE),
      ),
    '#element' => array ('#label_display' => 'inline'),
    '#label_display' => 'inline',
    ),
  );
  return $form;
}

/**
 * Helper function for cpm_indicator_field_widget_settings_form().
 *
 * @see cpm_indicator_field_widget_settings_form_validate()
 */
function _cpmindicator_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

// FRED DEFINE SMILECHECK FIELDS FORMAT OPTIONS

  $form = array();

  $form['label_position'] = array(
    '#type' => 'value',
    '#value' => $settings['label_position'],
  );
  
  return $form;
}

/**
 * Helper function for date_field_instance_settings_form().
 *
 * @see cpm_indicator_field_instance_settings_form_validate()
 */
function _cpmindicator_field_instance_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $instance['settings'];
  $widget_settings = $instance['widget']['settings'];
/*
  $form['smilies_options'] = array(
    '#type' => 'textarea',
    '#title' => t('Smilies Options'),
    '#default_value' => variable_get('smilies_options', ''),
    '#rows' => 10,
    '#description' => t('Describe <em>indicator options</em>'),
  );
*/

/*
  $form['trend'] = array(
      '#type' => 'checkbox',
      '#title' => t('Ask for trend'),
      '#default_value' => variable_get('trend', TRUE),
      '#description' => t('Ask for trend along with current state'),
    );
*/

/*
  $form['trends_options'] = array(
    '#type' => !empty($field['settings']['trend']) ? 'textarea' : 'hidden',
    '#title' => t('Trends options'),
    '#default_value' => variable_get('smilies_options', ''),
    '#rows' => 10,
    '#description' => t('describe trend options'),
  );
*/

  if (!empty($field['settings']['comment'])) {
    $form['text_processing'] = array(
      '#type' => 'radios',
      '#title' => t('Text processing'),
      '#default_value' => isset($settings['text_processing']) ? $settings['text_processing'] : 0,
      '#options' => array(
        t('Plain text'),
        t('Filtered text (user selects text format)'),
      ),
    );
  }
/*
  $form['comment'] = array(
      '#type' => 'checkbox',
      '#title' => t('Ask for per indicator comment'),
      '#default_value' => variable_get('trend', TRUE),
      '#description' => t('Suggests to leave a comment for each indicator'),
    );
*/

  $context = array(
    'field' => $field,
    'instance' => $instance,
  );
  drupal_alter('monitoring_field_instance_settings_form', $form, $context);

  return $form;
}

/**
 * Form validation handler for _cpmindicator_field_settings_form().
 */
function cpm_indicator_field_settings_validate(&$form, &$form_state) {
  $field = &$form_state['values']['field'];

  $field['settings']['value_set'] = $field['settings']['value_box']['value_set'];
  // Don't save the pseudo values created in the UI.
  unset($field['settings']['value_box']['value_set']);

  // Extract the correct 'trend' value out of the two trend checkboxes.
  if ($field['settings']['trend_box']['trend_get']) {
    if ($field['settings']['trend_box']['trend_required']) {
      $field['settings']['trend'] = 'required';
    }
    else {
      $field['settings']['trend'] = 'optional';
    }
  }
  else {
    $field['settings']['trend'] = '';
  }
  // Don't save the pseudo values created in the UI.
  unset($field['settings']['trend_box']['trend_get'], $field['settings']['trend_box']['trend_required']);

  // Extract the correct 'comment' value out of the two comment checkboxes.
  if ($field['settings']['comment_box']['comment_get']) {
    if (is_string($field['settings']['comment_box']['comment_required'])) {
      $field['settings']['comment'] = $field['settings']['comment_box']['comment_required'];
    }
    else{
      if ($field['settings']['comment_box']['comment_required']) {
        $field['settings']['comment'] = 'required';
      }
      else {
        $field['settings']['comment'] = 'optional';
      }
    }
  }
  else {
    $field['settings']['comment'] = '';
  }

  // Don't save the pseudo values created in the UI.
  unset($field['settings']['comment_box']['comment_get'], $field['settings']['comment_box']['comment_required']);
}

