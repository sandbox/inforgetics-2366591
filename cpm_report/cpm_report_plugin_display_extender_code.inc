<?php
/**
 * @file
 * Contains the class to extend views display plugins browse all link.
 */

/**
 * The plugin that added additional setting to views edit form.
 *
 * @ingroup views_display_plugins
 */
class cpm_report_plugin_display_extender_code extends views_plugin_display_extender {
  /**
   * Provide a form to edit options for this plugin.
   */
  function option_definition() {
    $options['cpm_report'] = array('default' => FALSE, 'bool' => TRUE);
    return $options;
  }

  /**
   * Provide the form to set new option.
   */
  function options_form(&$form, &$form_state) {
    switch ($form_state['section']) {
      case 'cpm_report':
        $form['#title'] .= t('CPM Reports');
        $form['cpm_report'] = array(
          '#title' => 'Parse CPM Report',
          '#type' => 'checkbox',
          '#description' => t('Parse view output for comments'),
          '#default_value' => $this->display->get_option('cpm_report'),
        );
        break;
    }
  }
  
  /**
   * Inserts the code into the view display.
   */
  function options_submit(&$form, &$form_state) {
    $new_option  = $form_state['values']['cpm_report'];
    switch ($form_state['section']) {
      case 'cpm_report':
        $this->display->set_option('cpm_report', $new_option);
        $empty = trim($new_option);
        $empty = empty($empty);
        break;
    }
  }

  /**
   * Summarizes new option.
   *
   * Lists the fields as either 'Yes' if there is text or 'None' otherwise and
   * categorizes the fields under the 'Other' category.
   */
  function options_summary(&$categories, &$options) {
    $options['cpm_report'] = array(
      'category' => 'other',
      'title'    => t('Parse CPM Report'),
      'value'    => $this->display->get_option('cpm_report') ? t('Yes') : t('No'),
      'desc'     => t('Add some option.'),
    );
  }
}
