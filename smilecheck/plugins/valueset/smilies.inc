<?php

/**
 * @file
 * Smilies valueset
 */

$plugin = array(
  'title' => 'Smilies',
  'values' => array(
    'description' => t('the project is'),
    'options' => array(
      'TOP' => array (
        'int' => 2,
        'name' => t('exceptionally well'),
        'desc' => t('marked by an event particularly positive'),
        'require comment' => TRUE,
        'color' => '#66CCFF', // rgb(102, 204, 255)
        'icon' => 'smilies/evalTOP.gif',
      ),
      'OK' => array (
        'int' => 1,
        'name' => t('in good health'),
        'desc' => t('conform to expectations'),
        'color' => '#009900', // rgb(0, 153, 0)
        'icon' => 'smilies/evalOK.gif',
      ),
      'NOK' => array (
        'int' => -1,
        'name' => t('in difficulty'),
        'desc' => t('facing questions or incertainties perturbing advancement'),
        'require comment' => TRUE,
        'color' => '#FF9933', // rgb(255, 153, 51)
        'icon' => 'smilies/evalNOK.gif',
      ),
      'KO' => array (
        'int' => -2,
        'name' => t('in danger'),
        'desc' => t('facing problems that require urgent action or decision'),
        'require comment' => TRUE,
        'color' => '#FF0033', // rgb(255, 0, 51)
        'icon' => 'smilies/evalKO.gif',
      ),
    ),
  ),
  'trends' => array(
    'description' => t('and the trend is'),
    'options' => array(
      'up' => array (
        'int' => 2,
        'name' => t('positive'),
        'desc' => t('Good or better news are coming.'),
        'color' => '#339966',
        'icon' => 'trends/evol_UP.png',
        'bkgrnd' => 'trends/up.gif',
      ),
      'neutral' => array (
        'int' => 1,
        'name' => t('neutral'),
        'desc' => t('Current situation should go on.'),
        'color' => '#BBBBBB',
        'icon' => 'trends/evol_FLAT.png',
        'bkgrnd' => 'trends/neutral.gif',
      ),
      'down' => array (
        'int' => -1,
        'name' => t('negative'),
        'desc' => t('Worse news are expected if conditions do not change.'),
        'color' => '#CC3333',
        'icon' => 'trends/evol_DOWN.png',
        'bkgrnd' => 'trends/down.gif',
      ),
    ),
  ),
  'media' => array (
    'smilies' => array(
      'type' => 'picto',
      'title' => t('Smilies'),
    )
  ),
  'legend' => array(
    'block' => array(
      'name' => 'smiles_legend',
      'title' => 'Legend of the indicators',
      'description' => 'Legend',
      'body' => '<div class="form-item">
<p>The project is :</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:800px">
	<tbody>
		<tr>
			<td class="rtecenter" style="background-color: rgb(102, 204, 255);">exceptionally well</td>
			<td>marked by an event particularly positive.</td>
		</tr>
		<tr>
			<td class="rtecenter" style="background-color: rgb(0, 153, 0);">in good health</td>
			<td>conform to expectations.</td>
		</tr>
		<tr>
			<td class="rtecenter" style="background-color: rgb(255, 153, 51);">in difficulty</td>
			<td>facing questions or incertainties perturbing advancement.</td>
		</tr>
		<tr>
			<td class="rtecenter" style="background-color: rgb(255, 0, 51);">in danger</td>
			<td>facing problems that require urgent action or decision.</td>
		</tr>
	</tbody>
</table>

<p>The trend is :</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:600px">
	<tbody>
		<tr>
			<td class="cpmlegend_background_icon trend_up rtecenter" style="background-color: rgb(51, 153, 102);">positive</td>
			<td>Good or better news are coming.</td>
		</tr>
		<tr>
			<td class="cpmlegend_background_icon trend_neutral rtecenter" style="background-color: rgb(187, 187, 187);">neutral</td>
			<td>Current situation should go on.</td>
		</tr>
		<tr>
			<td class="cpmlegend_background_icon trend_down rtecenter" style="background-color: rgb(204, 51, 51);">negative</td>
			<td>Worse news are expected if conditions do not change.</td>
		</tr>
	</tbody>
</table>
</div>
',
      ),
    'field' => array(
      'name' => 'field_legend',
      'label' => 'Legend',
      'markup' => '<div class="form-item">
<p>For the technical, deadlines, budget, resources and involvement aspects, please set if the project is :</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:800px">
	<tbody>
		<tr>
			<td class="rtecenter" style="background-color: rgb(102, 204, 255);">exceptionally well</td>
			<td>marked by an event particularly positive.</td>
		</tr>
		<tr>
			<td class="rtecenter" style="background-color: rgb(0, 153, 0);">in good health</td>
			<td>conform to expectations.</td>
		</tr>
		<tr>
			<td class="rtecenter" style="background-color: rgb(255, 153, 51);">in difficulty</td>
			<td>facing questions or incertainties perturbing advancement.</td>
		</tr>
		<tr>
			<td class="rtecenter" style="background-color: rgb(255, 0, 51);">in danger</td>
			<td>facing problems that require urgent action or decision.</td>
		</tr>
	</tbody>
</table>

<p>Please add a comment when the project is <span style="color:#0000FF">exceptionaly well</span>, <span style="color:#FFA07A">in difficulty</span> or <span style="color:#FF0000">in danger</span>.</p>

<p>Can you also indicate the trend for these indicators:</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:600px">
	<tbody>
		<tr>
			<td class="cpmlegend_background_icon trend_up rtecenter" style="background-color: rgb(51, 153, 102);">positive</td>
			<td>Good or better news are coming.</td>
		</tr>
		<tr>
			<td class="cpmlegend_background_icon trend_neutral rtecenter" style="background-color: rgb(187, 187, 187);">neutral</td>
			<td>Current situation should go on.</td>
		</tr>
		<tr>
			<td class="cpmlegend_background_icon trend_down rtecenter" style="background-color: rgb(204, 51, 51);">negative</td>
			<td>Worse news are expected if conditions do not change.</td>
		</tr>
	</tbody>
</table>
</div>
',
      ),
  ),
);
