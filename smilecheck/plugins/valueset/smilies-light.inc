<?php

/**
 * @file
 * Smilies-Light valueset
 */

$plugin = array(
  'title' => 'Smilies Light',
  'values' => array(
    'description' => t('the project is'),
    'options' => array(
      'TOP' => array (
        'int' => 2,
        'name' => t('Top !'),
        'desc' => t('marked by an event particularly positive'),
        'require comment' => TRUE,
        'color' => '#93D14F', // rgb(147, 209, 79)
        'icon' => 'smilies-light/evalTOP.gif',
      ),
      'OK' => array (
        'int' => 1,
        'name' => t('OK'),
        'desc' => t('conform to expectations'),
        'color' => '#FC61FB', // rgb(252, 97, 251)
        'icon' => 'smilies-light/evalOK.gif',
      ),
      'NOK' => array (
        'int' => -1,
        'name' => t('middle'),
        'desc' => t('facing questions or incertainties perturbing advancement'),
        'require comment' => TRUE,
        'color' => '#FFFC06', // rgb(255, 252, 6)
        'icon' => 'smilies-light/evalNOK.gif',
      ),
      'KO' => array (
        'int' => -2,
        'name' => t('in difficulty'),
        'desc' => t('facing problems that require urgent action or decision'),
        'require comment' => TRUE,
        'color' => '#01B0F1', // rgb(1, 176, 241)
        'icon' => 'smilies-light/evalKO.gif',
      ),
    ),
  ),
  'trends' => array(
    'description' => t('and the trend is'),
    'options' => array(
      'up' => array (
        'int' => 2,
        'name' => t('positive'),
        'desc' => t('Good or better news are coming.'),
        'color' => '#339966',
        'icon' => 'trends/evol_UP.png',
        'bkgrnd' => 'trends/up.gif',
      ),
      'neutral' => array (
        'int' => 1,
        'name' => t('neutral'),
        'desc' => t('Current situation should go on.'),
        'color' => '#BBBBBB',
        'icon' => 'trends/evol_FLAT.png',
        'bkgrnd' => 'trends/neutral.gif',
      ),
      'down' => array (
        'int' => -1,
        'name' => t('negative'),
        'desc' => t('Worse news are expected if conditions do not change.'),
        'color' => '#CC3333',
        'icon' => 'trends/evol_DOWN.png',
        'bkgrnd' => 'trends/down.gif',
      ),
    ),
  ),
  'media' => array (
    'smilies-light' => array(
      'type' => 'picto',
      'title' => t('Smilies Light'),
    ),
    'smilies-light-w' => array(
      'type' => 'picto',
      'title' => t('Smiles Light White'),
    ),
  ),
  'legend' => array(
    'block' => array(
      'name' => 'smiles_light_legend',
      'title' => 'Legend of the indicators',
      'description' => 'Legend',
      'body' => '<div class="form-item">
<p>The project is :</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:800px">
	<tbody>
		<tr>
			<td class="rtecenter" style="background-color: rgb(147, 209, 79);">Top !</td>
			<td>marked by an event particularly positive.</td>
		</tr>
		<tr>
			<td class="rtecenter" style="background-color: rgb(252, 97, 251);">OK</td>
			<td>conform to expectations.</td>
		</tr>
		<tr>
			<td class="rtecenter" style="background-color: rgb(255, 252, 6);">middle</td>
			<td>facing questions or incertainties perturbing advancement.</td>
		</tr>
		<tr>
			<td class="rtecenter" style="background-color: rgb(1, 176, 241);">in difficulty</td>
			<td>facing problems that require urgent action or decision.</td>
		</tr>
	</tbody>
</table>

<p>The trend is :</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:600px">
	<tbody>
		<tr>
			<td class="cpmlegend_background_icon trend_up rtecenter" style="background-color: rgb(51, 153, 102);">positive</td>
			<td>Good or better news are coming.</td>
		</tr>
		<tr>
			<td class="cpmlegend_background_icon trend_neutral rtecenter" style="background-color: rgb(187, 187, 187);">neutral</td>
			<td>Current situation should go on.</td>
		</tr>
		<tr>
			<td class="cpmlegend_background_icon trend_down rtecenter" style="background-color: rgb(204, 51, 51);">negative</td>
			<td>Worse news are expected if conditions do not change.</td>
		</tr>
	</tbody>
</table>
</div>
',
      ),
    'field' => array(
      'name' => 'field_legend',
      'label' => 'Legend',
      'markup' => '<div class="form-item">
<p>For the technical, deadlines, budget, resources and involvement aspects, please set if the project is :</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:800px">
	<tbody>
		<tr>
			<td class="rtecenter" style="background-color: rgb(147, 209, 79);">top !</td>
			<td>marked by an event particularly positive.</td>
		</tr>
		<tr>
			<td class="rtecenter" style="background-color: rgb(252, 97, 251);">OK</td>
			<td>conform to expectations.</td>
		</tr>
		<tr>
			<td class="rtecenter" style="background-color: rgb(255, 252, 6);">middle</td>
			<td>facing questions or incertainties perturbing advancement.</td>
		</tr>
		<tr>
			<td class="rtecenter" style="background-color: rgb(1, 176, 241);">in difficulty</td>
			<td>facing problems that require urgent action or decision.</td>
		</tr>
	</tbody>
</table>

<p>Please add a comment when the project is <span style="color:#93D14F">top</span>, <span style="color:#FFFC06">middle</span> or <span style="color:#01B0F1">in difficulty</span>.</p>

<p>Can you also indicate the trend for these indicators:</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:600px">
	<tbody>
		<tr>
			<td class="cpmlegend_background_icon trend_up rtecenter" style="background-color: rgb(51, 153, 102);">positive</td>
			<td>Good or better news are coming.</td>
		</tr>
		<tr>
			<td class="cpmlegend_background_icon trend_neutral rtecenter" style="background-color: rgb(187, 187, 187);">neutral</td>
			<td>Current situation should go on.</td>
		</tr>
		<tr>
			<td class="cpmlegend_background_icon trend_down rtecenter" style="background-color: rgb(204, 51, 51);">negative</td>
			<td>Worse news are expected if conditions do not change.</td>
		</tr>
	</tbody>
</table>
</div>
',
      ),
  ),
);
