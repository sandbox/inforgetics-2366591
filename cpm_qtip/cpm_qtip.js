(function ($) {
  Drupal.behaviors.cpm_qtip = {
    attach: function(context, settings) {
      // initialize
      var initialState = $.cookie('cpm_qtip');
      var showOrHideTooltips = false
      if ( initialState == "cpm_qtip" ) {
        $('#cpm_qtip_toggle').toggleClass('cpm_qtip_on cpm_qtip_off');
        showOrHideTooltips = true;
      }
      // attach to menu item "Show/Hide Menu"
      //var showOrHideTooltips = ( initialState == "cpm_qtip" ) ? true : false;
      $('#cpm_qtip_toggle').click(function(){
        if ( showOrHideTooltips == true ) {
          // div#region-content.grid-11
          //$('aside#region-sidebar-first').show();
        $('#cpm_qtip_toggle').toggleClass('cpm_qtip_on cpm_qtip_off');
          $.cookie('cpm_qtip', 'cpm_print', { path: settings.basePath, expires: 365 });
          showOrHideTooltips = false;
        }
        else if ( showOrHideTooltips == false ) {
          // div#region-content.grid-16
          //$('aside#region-sidebar-first').hide();
        $('#cpm_qtip_toggle').toggleClass('cpm_qtip_off cpm_qtip_on');
          $.cookie('cpm_qtip', 'cpm_qtip', { path: settings.basePath, expires: 365  });
          showOrHideTooltips = true;
        }
        location.reload();
        return false;
      });
    }
  };
})(jQuery);