<?php
/**
 * @file
 * Implements the display extender to add additional setting to views displays.
 */

/**
 * Implements hook_views_plugins().
 */
function cpm_report_views_plugins() { 
  $path = drupal_get_path('module', 'cpm_report');
  $plugins = array(); 
  $plugins['display_extender']['cpm_report'] = array( 
    'title' => t('CP Monitoring'), 
    'help' => t('Add option for CP Monitoring.'), 
    'path' => $path, 
    'handler' => 'cpm_report_plugin_display_extender_code',
  ); 
  return $plugins; 
}
