<?php
/**
 * @file
 * cpm_indicator forms and form themes.
 *
 * All code used in form editing and processing is in this file,
 * included only during form editing.
 */

/**
 * Private implementation of hook_widget().
 *
  */
function cpm_indicator_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $base) {

  $element = $base;
  $field_name = $field['field_name'];
  $entity_type = $instance['entity_type'];

  $element += array(
    '#type' => 'cpmindicator_combo',
    '#theme_wrappers' => array('cpmindicator_combo'),
    '#weight' => $delta,
    '#default_value' => isset($items[$delta]) ? $items[$delta] : '',
    '#element_validate' => array('cpmindicator_combo_validate'),

    // Store the original values, for use with disabled and hidden fields.
    '#cpmindicator_items' => isset($items[$delta]) ? $items[$delta] : '',
  );

  $element['#title'] = $instance['label'];

  return $element;
}

/**
 * Helper function to figure out the bundle name for an entity.
 */
function cpm_indicator_get_entity_bundle($entity_type, $entity) {
  switch ($entity_type) {
    case 'field_collection_item':
      $bundle = $entity->field_name;
      break;
    default:
      $bundle = field_extract_bundle($entity_type, $entity);
      break;
  }
  // If there is no bundle name, field_info() uses the entity name as the bundle
  // name in its arrays.
  if (empty($bundle)) {
    $bundle = $entity_type;
  }
  return $bundle;
}

/**
 * Process an individual cpmindicator element.
 */
function cpmindicator_combo_element_process($element, &$form_state, $form) {
  $field_name = $element['#field_name'];
  $delta = $element['#delta'];
  $bundle = $element['#bundle'];
  $entity_type = $element['#entity_type'];
  $langcode = $element['#language'];

  $field = field_widget_field($element, $form_state);
  $instance = field_widget_instance($element, $form_state);
  $valueset_id = $field['settings']['value_set'];
  // Figure out how many items are in the form, including new ones added by ajax.
  $field_state = field_form_get_state($element['#field_parents'], $field_name, $element['#language'], $form_state);
  $items_count = $field_state['items_count'];

  $columns = $element['#columns'];

  $value_field = 'value';
  $trend_field = 'trend';
  $comment_field = 'comment';

  $parents = $element['#parents'];
  $first_parent = array_shift($parents);
  $show_id = $first_parent . '[' . implode('][', $parents) . ']';

  $element[$value_field] = array(
    '#field'         => $field,
    '#instance'      => $instance,
    '#weight'        => $instance['widget']['weight'],
    '#required'      => ($instance['required'] && $delta == 0) ? 1 : 0,
    '#default_value' => isset($element['#default_value'][$value_field]) ? $element['#default_value'][$value_field] : NULL,
    '#delta'         => $delta,
    );

  $description =  !empty($instance['description']) ? $instance['description'] : '';

//  $element[$value_field]['#description'] = t('the project is');
  $element[$value_field]['#cpmindicator_element'] = TRUE;
  $element[$value_field]['#attributes'] = array('class' => array('cpmindicator-clear'));
  $element[$value_field]['#wrapper_attributes'] = array('class' => array());
  $element[$value_field]['#wrapper_attributes']['class'][] = 'cpmindicator-no-float';

  $base = $element[$value_field];

  switch ($instance['widget']['type']) {
/*
    case 'weather':
      $element[$value_field]['#type'] = 'select';
//      $element[$value_field]['#theme_wrappers'] = array('cpmindicator');
      $element['#attached']['js'][] = drupal_get_path('module', 'cpm_indicator') . '/cpm_indicator.js';
//      $element[$value]['#ajax'] = !empty($element['#ajax']) ? $element['#ajax'] : FALSE;
      break;
    case 'smilies':
*/
    default:
      $element[$value_field]['#title'] = t('the project is');
      $element[$value_field]['#type'] = 'radios';
      $element[$value_field]['#options'] = cpm_indicator_valueset_values_options($valueset_id, 'values');
      $element[$value_field]['#required_error'] = t('A value is required for <i>Current State</i> of %field aspect.', array('%field' => $element['#title']));
//      $element[$value_field]['#cpmindicator_element'] = TRUE;
      $element[$value_field]['#wrapper_attributes']['class'][] = 'value-wrapper';
      $element[$value_field]['#theme_wrappers'] = array('cpmindicator_form_element_radio');
      $element['#attached']['js'][] = drupal_get_path('module', 'cpm_indicator') . '/cpm_indicator.js';
//      $element[$value]['#ajax'] = !empty($element['#ajax']) ? $element['#ajax'] : FALSE;
      break;
  }

  // If this field uses the 'End', add matching element
  // for the 'End' date, and adapt titles to make it clear which
  // is the 'Start' and which is the 'End' .

  if (!empty($field['settings']['trend'])) {
    $element[$trend_field] = $base;
    $element[$trend_field]['#type'] = 'radios';
//    $element[$value_field]['#title'] = '';
    $element[$trend_field]['#options'] = cpm_indicator_valueset_values_options($valueset_id, 'trends');
//      $element[$trend_field]['#cpmindicator_element'] = TRUE;
    $element[$trend_field]['#theme_wrappers'] = array('cpmindicator_form_element_radio');
//     unset($element[$trend_field]['#theme_wrappers']);
//      $element[$trend_field]['#type'] = 'select';

    $element[$trend_field]['#title'] = t('and the trend is');
//    $element[$trend_field]['#description'] = t('and the trend is');
    $element[$trend_field]['#wrapper_attributes']['class'][] = 'trend-wrapper';
    $element[$trend_field]['#default_value'] = isset($element['#default_value'][$trend_field]) ? $element['#default_value'][$trend_field] : NULL;
    $element[$trend_field]['#required_error'] = t("A value is required for the <i>Trend</i> of %field aspect.", array('%field' => $element['#title']));
    $element[$trend_field]['#required'] = ($element[$value_field]['#required'] && $field['settings']['trend'] == 'required');
    $element[$trend_field]['#weight'] += .2;
    $element[$trend_field]['#prefix'] = '';
    $element['#fieldset_description'] = $description;
  }

  if (!empty($field['settings']['comment'])) {
    $element[$comment_field] = $base;
    unset($element[$comment_field]['#options']);

    $element[$comment_field]['#title'] = t('Comment');
//    $element[$comment_field]['#description'] = t('Comment:');
    $element[$comment_field]['#type'] = 'textarea';
    $element[$comment_field]['#cols'] = 120;
    $element[$comment_field]['#rows'] = 5;

    if ($instance['settings']['text_processing']) {
      $element[$comment_field]['#type'] = 'text_format';
//      $element[$comment_field]['#format'] = isset($items[$delta]['comment_format']) ? $items[$delta]['comment_format'] : NULL;
      $element[$comment_field]['#format'] = isset($element['#default_value']['comment_format']) ? $element['#default_value']['comment_format'] : NULL;
//      $element[$comment_field]['#base_type'] = $element[$comment_field]['#type'];
    }

//    $element[$comment_field]['#default_value'] = isset($items[$delta]['comment_value'])?$items[$delta]['comment_value']:'',
//    $element[$comment_field]['#default_value'] = isset($element['#default_value'][$comment_field]) ? $element['#default_value'][$comment_field] : '';
    $element[$comment_field]['#default_value'] = isset($element['#default_value']['comment_value']) ? $element['#default_value']['comment_value'] : '';
    $element[$value_field]['#wrapper_attributes']['class'][] = 'comment-wrapper';
    $element[$comment_field]['#wrapper_attributes']['class'][] = 'comment-wrapper';
    $element[$comment_field]['#required'] = ($element[$value_field]['#required'] && $field['settings']['comment'] == 'required');
    $element[$comment_field]['#weight'] += .4;
    $element[$comment_field]['#prefix'] = '';

  }

  // Create label for error messages that make sense in multiple values
  // and when the title field is left blank.
    $element[$value_field]['#cpmindicator_title'] = $instance['label'];
    $element[$trend_field]['#cpmindicator_title'] = $instance['label'];
    $element[$comment_field]['#cpmindicator_title'] = $instance['label'];
//  }

  $context = array(
   'field' => $field,
   'instance' => $instance,
   'form' => $form,
  );
  drupal_alter('cpmindicator_combo_process', $element, $form_state, $context);
  return $element;
}

function cpmindicator_element_empty($element, &$form_state) {
  $item = array();
  $item['value'] = NULL;
  $item['trend'] = NULL;
  $item['comment_value'] = NULL;
  form_set_value($element, $item, $form_state);
  return $item;
}

/**
 * Validate and update a combo element.
 * Don't try this if there were errors before reaching this point.
 */
function cpmindicator_combo_validate($element, &$form_state) {

  // Disabled and hidden elements won't have any input and don't need validation,
  // we just need to re-save the original values, from before they were processed into
  // widget arrays and timezone-adjusted.
/*
  if (cpmindicator_hidden_element($element) || !empty($element['#disabled'])) {
    form_set_value($element, $element['#cpmindicator_items'], $form_state);
    return;
  }
*/
  $field_name = $element['#field_name'];
  $delta = $element['#delta'];
  $langcode = $element['#language'];

  $form_values = drupal_array_get_nested_value($form_state['values'], $element['#field_parents']);
  $form_input = drupal_array_get_nested_value($form_state['input'], $element['#field_parents']);

  // If the whole field is empty and that's OK, stop now.
  if (empty($form_input[$field_name]) && !$element['#required']) {
    return;
  }

  $item = $form_values[$field_name][$langcode][$delta];
  $posted = $form_input[$field_name][$langcode][$delta];

  $field = field_widget_field($element, $form_state);
  $instance = field_widget_instance($element, $form_state);

  if (is_array($form_values[$field_name][$langcode][$delta]['comment'])){
    $form_state['values'][$field_name][$langcode][$delta]['comment_value'] = $form_values[$field_name][$langcode][$delta]['comment']['value'];
    $form_state['values'][$field_name][$langcode][$delta]['comment_format'] = $form_values[$field_name][$langcode][$delta]['comment']['format'];
  }else{
  $form_state['values'][$field_name][$langcode][$delta]['comment_value'] = $form_values[$field_name][$langcode][$delta]['comment'];
  }


  $context = array(
    'field' => $field,
    'instance' => $instance,
    'item' => $item,
  );

  drupal_alter('cpmindicator_combo_pre_validate', $element, $form_state, $context);

  $value_field = 'value';
  $trend_field = 'trend';
  $comment_field = 'comment';

  // Check for empty 'Value', which could either be an empty
  // value or an array of empty values, depending on the widget.
  $empty = TRUE;
  if (!empty($item[$value_field])) {
    if (!is_array($item[$value_field])) {
      $empty = FALSE;
    }
    else {
      foreach ($item[$value_field] as $key => $value) {
        if (!empty($value)) {
          $empty = FALSE;
          break;
        }
      }
    }
  }

/* Done through element settings (required_error) + ! needs a patch for 'drupal includes/form.inc'
  if ($empty) {
    $item = cpmindicator_element_empty($element, $form_state);
    if (!$element['#required']) {
      return;
    }else{
      form_error($element, t("A value is required for current 'State' of %field aspect.", array('%field' => $instance['label'])));
    }
  }

    if ($field['settings']['trend'] == 'required' && empty($item[$trend_field])) {
  //          form_error($element, t("A 'Trend' value is required for field %field #%delta", array('%delta' => $field['cardinality'] ? intval($delta + 1) : '', '%field' => $instance['label'])));
            form_error($element, t("A 'Trend' value is required for %field aspect.", array('%field' => $instance['label'])));
        $empty = FALSE;
    }
*/
  if(!$empty){
    if ($field['settings']['comment'] == 'if_not_ok' && empty($item[$comment_field])) {
      $bundle=$instance['bundle'];
      $value_set=$field['settings']['value_set'];
      $option_def = cpm_indicator_value_set($bundle, $value_set);
      $value_def = $option_def['values']['options'][$item[$value_field]];
      if(isset($value_def['require comment']) && $value_def['require comment']){
        $status =  $value_def['name'];
  //          form_error($element, t("Please add a 'Comment' about what is '%status' in %field #%delta aspect", array('%delta' => $field['cardinality'] ? intval($delta + 1) : '', '%field' => $instance['label'], '%status' => $status)));
            form_error($element, t("Please add a <em>Comment</em> about what is '%status' in %field aspect.", array('%field' => $instance['label'], '%status' => $status)));
        $empty = FALSE;
        }
    }
  }

  // A 'Trend' without a 'Value' is a validation error.
  if ($empty && !empty($item[$trend_field])) {
    if (!is_array($item[$trend_field])) {
//      form_error($element, t("A 'Current' value is required if a 'Trend' is supplied for field %field #%delta.", array('%delta' => $field['cardinality'] ? intval($delta + 1) : '', '%field' => $instance['label'])));
      form_error($element, t("A value is required for <i>Current state</i> of %field aspect if a <i>Trend</i> is supplied.", array('%field' => $instance['label'])));
      $empty = FALSE;
    }
    else {
      foreach ($item[$trend_field] as $key => $value) {
        if (!empty($value)) {
//          form_error($element, t("A 'Current' value is required if a 'Trend' is supplied for field %field #%delta.", array('%delta' => $field['cardinality'] ? intval($delta + 1) : '', '%field' => $instance['label'])));
          form_error($element, t("A value is required for <i>Current state</i> of %field aspect if a <i>Trend</i> is supplied.", array('%field' => $instance['label'])));
          $empty = FALSE;
          break;
        }
      }
    }
  }


  /*
  if (!empty($errors)) {
    if ($field['cardinality']) {
      form_error($element, t('There are errors in @field_name value #@delta:', array('@field_name' => $instance['label'], '@delta' => $delta + 1)) . theme('item_list', array('items' => $errors)));
    }
    else {
      form_error($element, t('There are errors in @field_name:', array('@field_name' => $instance['label'])) . theme('item_list', array('items' => $errors)));
    }
  }
  */
}

/**
 * Implements hook_cpmindicator_select_pre_validate_alter().
 */
function cpmindicator_cpmindicator_select_pre_validate_alter(&$element, &$form_state, &$input) {
  cpmindicator_empty_trend($element, $form_state, $input);
}

/**
 * Implements hook_cpmindicator_option_pre_validate_alter().
 */
function cpmindicator_cpmindicator_option_pre_validate_alter(&$element, &$form_state, &$input) {
  cpmindicator_empty_trend($element, $form_state, $input);
}

/**
 * Helper function to clear out trend when not being used.
 */
function cpmindicator_empty_trend(&$element, &$form_state, &$input) {
  // If this is the trend and the option to show a trend has not been selected,
  // empty the trend to surpress validation errors and stop further processing.
  $parents = $element['#parents'];
  $parent = array_pop($parents);
  if ($parent == 'trend') {
    $parent_values = drupal_array_get_nested_value($form_state['values'], $parents);
    if (isset($parent_values['show_trend']) && $parent_values['show_trend'] != 1) {
      $input = array();
      form_set_value($element, NULL, $form_state);
    }
  }
}
